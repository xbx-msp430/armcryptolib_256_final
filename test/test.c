#include "crypto_hash.h"
#include "stdint.h"

#ifndef HOST 
#include <msp430fg4618.h>
#endif

#include <string.h>

#define RTCNT (*(uint32_t*) &RTCNT1)
const uint8_t data[8] = {
    0xCC, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07
};

int main(void){

    uint8_t hashval[32];
    uint32_t i;
    uint32_t last_timer;

    WDTCTL = WDTPW + WDTHOLD;
    RTCCTL = 0x6C; // Enable counter mode from SMCLK, no interrupts since 32 bit won't overflow at 1MHz
                   // SMCLK ~= 1MHz
                   // Counter stopped
    while(1){
        memset(hashval,0,32);
        RTCNT = 0;
        RTCCTL = 0x2C; //Enable Counter
        crypto_hash(hashval, (void*)data, 1);
        RTCCTL = 0x6C; //Disable Counter
                       //Break and read RTCCTL here
        //Should be
        // 0xEEAD6DBFC7340A56CAEDC044696A168870549A6A7F6F56961E84A54BD9970B8A

        last_timer = RTCNT;
        last_timer = 0;
    }
    return 0;
}



